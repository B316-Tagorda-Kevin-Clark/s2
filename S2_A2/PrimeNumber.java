package com.zuitt.activity.S2_A2;
import java.util.ArrayList;
import java.util.HashMap;


public class PrimeNumber {
    public static void main(String[] args) {
        int[] primeNumbers = new int[5];

        int count = 0;
        int number = 1;

        while (count < 1) {
            if (isPrime(number)) {
                primeNumbers[count] = number;
                count++;
            }
            number++;
        }

        System.out.println("The first prime number is: " + primeNumbers[0]);



        ArrayList<String> friends = new ArrayList();

        friends.add("John");
        friends.add("Jane");
        friends.add("Chloe");
        friends.add("Zoey");

        System.out.println("My Friends are: " + friends);

        HashMap<String, String>inventory = new HashMap();

        //Adding elements into the HashMap
        inventory.put("toothpaste", "15");
        inventory.put("toothbrush", "20");
        inventory.put("soap", "12");

        System.out.println("Our current inventory consists of" + inventory);



    }
    public static boolean isPrime(int num) {
        if (num <= 1) {
            return false;
        }

        for (int i = 2; i <= Math.sqrt(num); i++) {
            if (num % i == 0) {
                return false;
            }
        }

        return true;



    }
}
